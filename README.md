# Resurs Bank - Magento module

## Description

Metadata package to install all available Resurs Bank modules for Magento.

#### 2.1.5

* Fix appearance of Cost List widget.
* Fix translator performance.
* Fix translator handling of missing translation for requested language.
* Hide Konsumentverket warning for Resurs Invoice payment types.
* Hide price signage on non-internal payment methods.  

#### 2.1.4

* Improve translation performance.

#### 2.1.3

* Fix test callbacks.

#### 2.1.2

* Updated styling and appearance of product and checkout page warnings.

#### 2.1.1

* Fix product image placement bug on product pages.
* Fix SECCI link.

#### 2.1.0

* Add Konsumentverket warning on product pages and in checkout for Swedish customers.

#### 2.0.3

* Improve phone number validation.
* Fix localization bug in admin.
* Fix store selector in admin.

#### 2.0.2

* Localization improvements
* Add some tests
* Fetch address changes
* Minor fixes

#### 2.0.1

* Minor fixes, including fixing deployment of static content.

#### 2.0.0

* Includes magento-mapi module which introduces support for the Resurs Merchant API.

#### 1.2.0

* Updated version requirements of ordermanagement and core packages, both including code execution improvements.
